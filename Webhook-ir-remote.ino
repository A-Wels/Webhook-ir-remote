// Web server basics: https://randomnerdtutorials.com/esp8266-web-server/

#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <ESP8266WiFi.h>

const uint16_t LED_PIN = 4;  // ESP8266 GPIO pin 

IRsend irsend(LED_PIN);  // Set the GPIO to be used to sending the message.

// HEX codes of LED remote
const unsigned long code_red = 0xF700FF;
const unsigned long code_off = 0xF7B847;
const unsigned long code_yellow = 0xF708F7;
const unsigned long code_green = 0xF7807F;
const unsigned long code_on = 0xF738C7 ;
const unsigned long code_ambience = 0xF740BF;

const char* ssid = "";
const char* password = "";

//Server an Port 80
WiFiServer server(80);

// Aktuelle Zeit
unsigned long currentTime = millis();
// Letzte Zeit
unsigned long previousTime = 0; 
// Timeout in ms (example: 2000ms = 2s)
const long timeoutTime = 2000;
String header;

void setup() {
  irsend.begin();
  Serial.begin(115200);

  Serial.print("Verbinde mit WLAN:");
  Serial.println(ssid);
  //Hostname für Einbindung ins VisionVersum
  WiFi.setHostname("LED-Controller-1");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("\n WLAN verbunden.");
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void red_alert(){
  Serial.println("Roter Alarm!");
  irsend.sendNEC(code_red);
  delay(2000);
}

void turn_off(){
  Serial.println("Licht aus!");
  irsend.sendNEC(code_off);
  delay(2000);
}

void turn_on(){
  Serial.println("Licht ein");
  irsend.sendNEC(code_on);
  delay(2000);
}

void enable_ambience(){
  Serial.println("Normalzustand.");
  irsend.sendNEC(code_ambience);
  delay(2000);
}

void green(){
  Serial.println("Grün!");
  irsend.sendNEC(code_green);
  delay(2000);
}
void yellow_alert(){
  Serial.println("Gelber Alarm!");
  irsend.sendNEC(code_yellow);
  delay(2000);
}
void loop() {

 WiFiClient client = server.available();  

  if (client) {                             
    Serial.println("Neuer Client");         
    String currentLine = "";              
    currentTime = millis();
    previousTime = currentTime;
    while (client.connected() && currentTime - previousTime <= timeoutTime) { // loop while the client's connected
      currentTime = millis();         
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIO on and off

            if (header.indexOf("GET /red") >= 0) {
              Serial.println("Roter Alarm");
              red_alert();
            }
            if (header.indexOf("GET /yellow") >= 0) {
              Serial.println("Gelber Alarm");
              yellow_alert();
            }
            if (header.indexOf("GET /ambience") >= 0) {
              Serial.println("Ambience");
              enable_ambience();
            }
            if (header.indexOf("GET /off") >= 0) {
              Serial.println("Licht aus");
              turn_off();
            }
            if (header.indexOf("GET /lichtan") >= 0) {
              Serial.println("Licht an");
              turn_on();
        }            
        if (header.indexOf("GET /green") >= 0) {
              Serial.println("Gr&uuml;n");
              green();
            }
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #77878A;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>Lichtcontroller Br&uuml;cke</h1>");
            
            // Display Button to toggle tv
            
              client.println("<p><a href=\"/yellow\"><button class=\"button\">Gelber Alarm</button></a></p>");
              client.println("<p><a href=\"/red\"><button class=\"button\">Roter Alarm</button></a></p>");
              client.println("<p><a href=\"/ambience\"><button class=\"button\">Ambience</button></a></p>");
              client.println("<p><a href=\"/off\"><button class=\"button\">Licht aus</button></a></p>");
              client.println("<p><a href=\"/lichtan\"><button class=\"button\">Licht an</button></a></p>");
              client.println("<p><a href=\"/green\"><button class=\"button\">Gr&uuml;n</button></a></p>");

            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client getrennt.");
    Serial.println("");
  }
 
}
